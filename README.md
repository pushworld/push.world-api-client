push-world-api
==================

Библиотека PHP для работы с API сервиса [Push World](https://push.world/)

Требования
-------
PHP 5.4.0+

Установка
-------

Установите Composer http://getcomposer.org/ и выполните команду:

```
composer require pushworld/api-client dev-master
```

Примеры
-------

#### Отправить push-уведомление

```php
<?php
use pushworld\api\PushWorldApi;

/**
 * new PushWorldApi($clientId, $clientSecret, [ $filesPath ])
 * 
 * $clientId      - string, обязательный, идентификатор клиента;
 * $clientSecret  - string, обязательный, секретный ключ клиента;
 * $filesPath     - string, путь для хранения файла, содержащего access_token,
 *                  если он не указан - файл хранится в директории временных файлов,
 *                  путь к которой библиотека получает функцией sys_get_temp_dir, 
 *                  Имя этого файла - md5 хэш из конкатенации clientId и clientSecret.
 */

$api = new PushWorldApi($clientId, $clientSecret);

/**
 * multicastSend($platform_code, $multicast, [ $subscribers ])
 *
 * $platform_code - string, обязательный, идентификатор площадки
 *                  вида e013a8690a25d1f44d6bdabc39fc8f463d6010abc0b169fb74cbd9a3d93ae922
 * $multicast     - array, обязательный, где:
 *     title         - string, обязательный, заголовок уведомления;
 *     text          - string, обязательный, текст уведомления;
 *     url           - string, обязательный, URL, по которому осуществляется переход
 *                     при клике по уведомлению;
 *     image         - string, путь к изображению,
 *                     если не указан - используется изображение по-умолчанию,
                       заданное при создании площадки;
 *     action1_title - string, текст кнопки один;
 *     action1_url   - string, URL кнопки один;
 *     action2_title - string, текст кнопки два;
 *     action2_url   - string, URL кнопки два;
 *     duration      - int, время отображения уведомления на экране в секундах;
 *     life_time     - int, время жизни уведомления в секундах;
 *     image_large   - string, путь к изображению, если необходимо показывать большое изображение на уведомлении
 *
 * $subscribers   - array, массив строк, содержащий device_id подписчиков, которым необходимо доставить уведомление.
 *                  если не указано, то уведомление придет всем подписчикам площадки.
 *                  device_id - строка вида 8ffc9a7b-3948-4de9-9a5a-8853abcc7ac3
 *                              или PW.v1.1465379869368.GDxfGZ4oNQvHT929924333153
 */

$multicast = array(
    'title' => 'Заголовок уведомления',
    'text'  => 'Текст уведомления',
    'url'   => 'URL на который совершается переход при клике по уведомлению'
);

// пример отправки уведомления одному подписчику
$subscribers = array(
    '8ffc9a7b-3948-4fe9-9a5a-885acb4c7ac3'
);

// пример отправки уведомления нескольким подписчикам
$subscribers = array(
    '8ffc9a7b-3948-4fe9-9a5a-885acb4c7ac3',
    'dd23ac89-4698-329f-4bcd-290cef8f4da6',
    '9dfc9a7b-9541-dbf0-45ac-428dfe6735ac'
);


$api->multicastSend($platformCode, $multicast, $subscribers);

?>

```
Хранение access token
-------
В текущей версии библиотеки access token сохраняется в файл, находящийся по пути, указанном аргументом *$filesPath* в конструкторе PushWorldApi, если путь не указан - файл сохраняется в директории временных файлов.
Пользователь, от имени которого работает веб-сервер, должен иметь права на чтение и запись в соответствующей директории.